# Generic DAPS UI

This repository contains a generic UI for the TSG DAPS. Which can be tailored to the specific needs by configuring the look and feel via an external file.

## Development

This UI is a Vue-based application with Buefy support.

To setup your NodeJS environment for development execute:
```
npm install
```

In order to run a local version of the UI, update the `vue.config.js` file to connect to a running instance of the DAPS server, and then execute:
```
npm run serve
```

To build the project into a compiled version that can be used in a webserver, execute:
```
npm run build
```
The resulting build will be put into the `/dist` folder.

Also, a Docker file is provided to create a Docker image containing an Nginx server with the UI. With the possibility to use the `DAPS_ACCESSURL` environment variable to route traffic towards the DAPS server instance.

## Structure

The Vue app is constructed around a single-page layout in `App.vue`, with four different views:
- `Home`: The home page with some generic information on this instance of the DAPS.
- `Participants`: The registered participant certificates.
- `Components`: The registered component certificates.
- `Management`: The management interface for both participating parties as administrators.

The management interface is split up into several components given the different responsibilities in this view:
- `Login`: The login form, as well as the forms for registration and password reset.
- `ComponentMgmtTable`: The table containing a list of the components, if the user is an administrator the table also contains buttons for managing the certificate.
- `ComponentModal`: The modal containing the form for registering component certificates, with the possibility to create a keypair with certificate signing request.
- `ParticipantMgmtTable`: The table containing a list of the participants, if the user is an administrator the table also contains buttons for managing the certificate.
- `ParticipantModal`: The modal containing the form for registering participant certificates, with the possibility to create a keypair with certificate signing request.
- `UserMgmt`: The table of registered users, which can be used by administrators to manage users.
- `FederatedDaps`: The configuration of the Federated DAPS functionality of the DAPS server.

## Config file
The UI can be tailored to the specific dataspace the DAPS is deployed in by creating a `/config/config.json` file. Which can be, for instance, injected via a Kubernetes Config Map into `/app/config/config.json` in the generated Docker image.

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| `title` | String | `TSG Identity Provider` | Title of the identity provider as displayed in the UI titles |
| `logo` | URL | `/TSG-Logo-White.svg` | Link towards the logo in the header hero |
| `favicon` | URL | - | Link towards the favicon |
| `colors` | `{ [key: string]: string }` | - | Color overrides for the UI, will be set as `--color-KEY` property on the CSS root. Keys used in the UI are `accent`, `accent-darken`, `dark`, `header`, `body`, `hero-text` |
| `home.subtitle` | String | `Identities for IDS entities` | Subtitle that will be shown on the home page |
| `home.paragraphs` | String[] | `[]` | Paragraphs describing the instance of this identity provider |
| `term.participant` | String | `Participant` | Term used for participant |
| `term.participants` | String | `Participants` | Term used for participants |
| `term.component` | String | `Connector` | Term used for component |
| `term.components` | String | `Connectors` | Term used for components |
| `prefixes.participant` | URI | `urn:tsg:participants:` | Prefix for participants identifiers as shown in the placeholder of forms |
| `prefixes.component` | URI | `urn:tsg:connectors:` | Prefix for components identifiers as shown in the placeholder of forms |
| `prefixes.participantRegex` | Regex | - | Regular expression for participant identifiers used in the forms to enforce compliance |
| `prefixes.componentRegex` | Regex | - | Regular expression for component identifiers used in the forms to enforce compliance |
| `contact.required` | Boolean | `true` | Require administrator intervention to accept a certificate |
| `contact.contact.name` | String | - | Name of the administrator |
| `contact.contact.email` | String | - | Email address of the administrator |
| `footer.logo` | URL | `/TSG-Logo-White.svg` | Link towards the logo in the footer |
| `footer.copyright` | String | `&copy; 2023 TNO - TSG` | Copyright text shown in the footer |
