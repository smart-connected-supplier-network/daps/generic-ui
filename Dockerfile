FROM node:14-alpine as builder

WORKDIR /app
COPY package* /app/
RUN npm install
COPY . .
RUN npm run build

FROM nginx

ADD default.conf.template /etc/nginx/conf.d/default.conf.template

CMD ["/bin/bash", "-c", "envsubst '$DAPS_ACCESSURL $LISTENING_PORT' < /etc/nginx/conf.d/default.conf.template > /etc/nginx/conf.d/default.conf && exec nginx -g 'daemon off;'"]

COPY --from=builder /app/dist /app
